package com.kilapuser.kilapuser;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunFragment extends Fragment {

    public AkunFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.tv_nama_akun)
    TextView tvNamaAkun;
    @BindView(R.id.tv_jk_akun)
    TextView tvJkAkun;
    @BindView(R.id.tv_nohp_akun)
    TextView tvNohpAkun;
    @BindView(R.id.tv_email_akun)
    TextView tvEmailAkun;
    @BindView(R.id.civ_image_akun)
    ImageView imgFotoAkun;
    @BindView(R.id.tv_alamat_akun)
    TextView tvAlamatAkun;
    @BindView(R.id.tv_username_akun)
    TextView tvUsernameAkun;


    public static final String FRAGMENT_AKUN = "FRAGMENT_AKUN";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)getActivity()).tvTitleToolbar.setText("AKUN SAYA");

        UserSession x = new UserSession(getActivity());
        tvNamaAkun.setText(x.getNama());
        tvJkAkun.setText(x.getJk());
        tvNohpAkun.setText(x.getNoHp());
        tvEmailAkun.setText(x.getEmail());
//        Picasso.get()
//                .load()
        if (x.getAlamat().isEmpty() || x.getAlamat().equals(null)){
            tvAlamatAkun.setText("-");
        } else {
            tvAlamatAkun.setText(x.getAlamat());
        }
        tvUsernameAkun.setText(x.getUsername());
        Picasso.with(getActivity()).load(URL.rootImage + x.getFoto())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imgFotoAkun, new Callback() {
                    @Override
                    public void onSuccess() {
//                        pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        pbImage.setVisibility(View.GONE);
                        imgFotoAkun.setImageResource(R.drawable.avatar);
                    }
                });


        return view;
    }

    @OnClick(R.id.btn_logout)
    public void btnLogout(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Logout");
        dialog.setIcon(R.drawable.ic_power_red_24dp);
        dialog.setMessage("Ingin keluar?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new UserSession(getActivity()).clearSession();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                ((MainActivity)getActivity()).finish();
                dialogInterface.dismiss();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    @OnClick(R.id.tv_edit_profile)
    public void editProfile(){
        Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_pass)
    public void pass(){
        Intent intent = new Intent(getActivity(), UbahPassActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        Log.e("akun", "resume)");
        super.onResume();
        UserSession x = new UserSession(getActivity());
        tvNamaAkun.setText(x.getNama());
        tvJkAkun.setText(x.getJk());
        tvNohpAkun.setText(x.getNoHp());
        tvEmailAkun.setText(x.getEmail());
//        Picasso.get()
//                .load()
        if (x.getAlamat().isEmpty() || x.getAlamat().equals(null)){
            tvAlamatAkun.setText("-");
        } else {
            tvAlamatAkun.setText(x.getAlamat());
        }
        tvUsernameAkun.setText(x.getUsername());
        Picasso.with(getActivity()).load(URL.rootImage + x.getFoto())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imgFotoAkun, new Callback() {
                    @Override
                    public void onSuccess() {
//                        pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        pbImage.setVisibility(View.GONE);
                        imgFotoAkun.setImageResource(R.drawable.avatar);
                    }
                });
    }

    @Override
    public void onPause() {
        Log.e("akun", "pause");
        super.onPause();
    }
}
