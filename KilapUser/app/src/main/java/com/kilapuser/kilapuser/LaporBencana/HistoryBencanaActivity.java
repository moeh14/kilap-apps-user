package com.kilapuser.kilapuser.LaporBencana;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kilapuser.kilapuser.Model.ModelHistoryBencana;
import com.kilapuser.kilapuser.R;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class HistoryBencanaActivity extends AppCompatActivity {
    @BindView(R.id.ll_history_null)
    LinearLayout llHistoryNull;
    @BindView(R.id.srl_history)
    SwipeRefreshLayout srlHistory;
    @BindView(R.id.rv_history)
    RecyclerView list_item;

    RecyclerView.LayoutManager layoutManager;
    List<ModelHistoryBencana>list_datas;
    AdapterHIstoryBencana adapterHIstoryBencana;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    private ConnectionDetector conn;
    private BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_bencana);
        ButterKnife.bind(this);

        adapterHIstoryBencana = new AdapterHIstoryBencana(HistoryBencanaActivity.this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("HISTORY BENCANA");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        conn = new ConnectionDetector(HistoryBencanaActivity.this);

        //modal koneksi
        init_modal_bd_dialog();

        //get history
        getHistory();
        srlHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHistory();
                srlHistory.setRefreshing(false);
            }
        });
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    private void getHistory() {
        adapterHIstoryBencana = new AdapterHIstoryBencana(HistoryBencanaActivity.this);
        if (adapterHIstoryBencana.getDatas() != null) {
            adapterHIstoryBencana.getDatas().clear();
        } else {
            adapterHIstoryBencana.setDatas(new ArrayList<ModelHistoryBencana>());
        }
        adapterHIstoryBencana.notifyDataSetChanged();

        layoutManager = new LinearLayoutManager(HistoryBencanaActivity.this);
        list_item.setLayoutManager(layoutManager);
        list_item.setAdapter(adapterHIstoryBencana);

        final ProgressDialog pDialog = new ProgressDialog(HistoryBencanaActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Tunggu Sebentar...");
        pDialog.show();
        llHistoryNull.setVisibility(View.GONE);

        if (conn.isConnected()) {
            String url = URL.historyBencana+new UserSession(getApplicationContext()).getIdAkun();
            AsyncHttpClient client = new AsyncHttpClient();

            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("history", responseBody.toString());
                        if (datas.getInt("response") == 1 && statusCode == 200) {
                            JSONArray data = datas.getJSONArray("data");
                            Gson gson = new Gson();
                            TypeToken<List<ModelHistoryBencana>> token = new TypeToken<List<ModelHistoryBencana>>(){};
                            list_datas = gson.fromJson(data.toString(), token.getType());
                            if (adapterHIstoryBencana.getDatas()!=null){
                                adapterHIstoryBencana.getDatas().clear();
                            }
                            adapterHIstoryBencana.setDatas(list_datas);
                            adapterHIstoryBencana.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (statusCode == 400) {
                        try {
                            JSONObject datas =  new JSONObject(new String(responseBody));
                            Log.e("history error", datas.toString());
                            if (datas.getInt("response") == 0) {
                                llHistoryNull.setVisibility(View.VISIBLE);
                            } else {
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(HistoryBencanaActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_alert);
                                dialog.setTitle("Kesalahan");
                                dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                dialog.setPositiveButton("Tetap disini", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();
                                llHistoryNull.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if(statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException) {
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(HistoryBencanaActivity.this);
                        dialog.setCancelable(false);
                        dialog.setIcon(R.drawable.ic_alert);
                        dialog.setTitle("Kesalahan");
                        dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                        dialog.setPositiveButton("Tetap disini", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        dialog.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                                dialogInterface.dismiss();
                            }
                        });
                        dialog.show();
                        llHistoryNull.setVisibility(View.VISIBLE);
                    } else {
                        bsdConn.show();
                        llHistoryNull.setVisibility(View.VISIBLE);
                    }
                    pDialog.dismiss();
                }
            });
        } else {
            bsdConn.show();
            llHistoryNull.setVisibility(View.VISIBLE);
            pDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHistory();
    }
}
