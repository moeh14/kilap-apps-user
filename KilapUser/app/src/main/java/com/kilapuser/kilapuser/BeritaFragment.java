package com.kilapuser.kilapuser;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kilapuser.kilapuser.Model.ModelBeritaBencana;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class BeritaFragment extends Fragment {
    public BeritaFragment() {
        // Required empty public constructor
    } @BindView(R.id.ll_data_null)
    LinearLayout llDataNull;
    @BindView(R.id.srl_data)
    SwipeRefreshLayout srlData;
    @BindView(R.id.rv_data)
    RecyclerView list_item;

    RecyclerView.LayoutManager layoutManager;
    List<ModelBeritaBencana> list_datas;
    AdapterBeritaBencana adapterBeritaBencana;

    private ConnectionDetector conn;
    private BottomSheetDialog bsdConn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_berita, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)getActivity()).tvTitleToolbar.setText("BERITA");

        conn = new ConnectionDetector(getActivity());

        //modal koneksi
        init_modal_bd_dialog();

        adapterBeritaBencana = new AdapterBeritaBencana(getActivity());

        //get data
        getBeritaBencana();
        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBeritaBencana();
                srlData.setRefreshing(false);
            }
        });
        return view;
    }

    private void getBeritaBencana() {
        adapterBeritaBencana = new AdapterBeritaBencana(getActivity());
        if (adapterBeritaBencana.getDatas() != null) {
            adapterBeritaBencana.getDatas().clear();
        } else {
            adapterBeritaBencana.setDatas(new ArrayList<ModelBeritaBencana>());
        }
        adapterBeritaBencana.notifyDataSetChanged();

        layoutManager = new LinearLayoutManager(getActivity());
        list_item.setLayoutManager(layoutManager);
        list_item.setAdapter(adapterBeritaBencana);

        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Tunggu Sebentar...");
        pDialog.show();
        llDataNull.setVisibility(View.GONE);

        if (conn.isConnected()) {
            String url = URL.beritaBencana;
            AsyncHttpClient client = new AsyncHttpClient();

            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("data", datas.toString());
                        if (datas.getInt("response") == 1 && statusCode == 200) {
                            JSONArray data = datas.getJSONArray("data");
                            Gson gson = new Gson();
                            TypeToken<List<ModelBeritaBencana>> token = new TypeToken<List<ModelBeritaBencana>>(){};
                            list_datas = gson.fromJson(data.toString(), token.getType());
                            if (adapterBeritaBencana.getDatas()!=null){
                                adapterBeritaBencana.getDatas().clear();
                            }
                            adapterBeritaBencana.setDatas(list_datas);
                            adapterBeritaBencana.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    if (statusCode == 400) {
                        try {
                            JSONObject datas =  new JSONObject(new String(responseBody));
                            Log.e("data error", datas.toString());
                            if (datas.getInt("response") == 0) {
                                llDataNull.setVisibility(View.VISIBLE);
                            } else {
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_alert);
                                dialog.setTitle("Kesalahan");
                                dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();
                                llDataNull.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if(statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException) {
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setCancelable(false);
                        dialog.setIcon(R.drawable.ic_alert);
                        dialog.setTitle("Kesalahan");
                        dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        dialog.show();
                        llDataNull.setVisibility(View.VISIBLE);
                    } else {
                        bsdConn.show();
                        llDataNull.setVisibility(View.VISIBLE);
                    }
                    pDialog.dismiss();
                }
            });
        } else {
            bsdConn.show();
            llDataNull.setVisibility(View.VISIBLE);
            pDialog.dismiss();
        }
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(getActivity());
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

}
