package com.kilapuser.kilapuser;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class DaftraAkunActivity extends AppCompatActivity {
    @BindView(R.id.tv_daftar)
    TextView tvDaftar;
    @BindView(R.id.et_nm_lengkap)
    EditText etNmLengkap;
    @BindView(R.id.sp_jns_kelamin)
    Spinner spJnsKelamin;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.ccp_no_hp)
    CountryCodePicker cppNoHp;
    @BindView(R.id.et_no_hp)
    EditText etNoHp;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_pass)
    EditText etPass;
    @BindView(R.id.et_konf_pass)
    EditText etKonfPass;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftra_akun);
        ButterKnife.bind(this);
        //modal koneksi
        init_modal_bd_dialog();

        conn = new ConnectionDetector(getApplicationContext());

        tvDaftar.setText(Html.fromHtml("<font>Sudah Punya Akun? <b>Login</b><font>"));

        Log.e("spinner", String.valueOf(spJnsKelamin.getSelectedItem()));

        spinnerDataJnsKelamin();
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    private void spinnerDataJnsKelamin() {
        List<String> labels = new ArrayList<String>();
        labels.add(0, "Pilih jenis kelamin");
        labels.add(1, "Laki - laki");
        labels.add(2, "Perempuan");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_list, labels);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spJnsKelamin.setAdapter(dataAdapter);
    }

    @OnClick(R.id.btn_daftar_akun)
    public void btnDaftar() {
        if (etNmLengkap.getText().toString().isEmpty() && spJnsKelamin.getSelectedItem().toString().equals("Pilih jenis kelamin") && etEmail.getText().toString().isEmpty() &&
                etNoHp.getText().toString().isEmpty() && etUsername.getText().toString().isEmpty() && etPass.getText().toString().isEmpty() &&
                etKonfPass.getText().toString().isEmpty()) {
            etNmLengkap.setError("Harap diisi");
            TextView errorText = (TextView)spJnsKelamin.getSelectedView();
            errorText.setError("Harap Diisi");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("Harap diisi");//changes the selected item text to this
            etEmail.setError("Harap diisi");
            etNoHp.setError("Harap diisi");
            etUsername.setError("Harap diisi");
            etPass.setError("Harap diisi");
            etKonfPass.setError("Harap diisi");
        } else if (etNmLengkap.getText().toString().isEmpty()) {
            etNmLengkap.setError("Harap diisi");
        } else if (spJnsKelamin.getSelectedItem().toString().equals("Pilih jenis kelamin")) {
            TextView errorText = (TextView)spJnsKelamin.getSelectedView();
            errorText.setError("Harap Diisi");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText("Harap diisi");//changes the selected item text to this
        } else if (etEmail.getText().toString().isEmpty()){
            etEmail.setError("Harap diisi");
        } else if (etNoHp.getText().toString().isEmpty()){
            etNoHp.setError("Harap diisi");
        } else if (etUsername.getText().toString().isEmpty()) {
            etUsername.setError("Harap diisi");
        } else if (etPass.getText().toString().isEmpty()) {
            etPass.setError("Harap diisi");
        } else if (etKonfPass.getText().toString().isEmpty()) {
            etKonfPass.setError("Harap diisi");
        } else if (!etPass.getText().toString().equals(etKonfPass.getText().toString()) || !etKonfPass.getText().toString().equals(etPass.getText().toString())) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(DaftraAkunActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Silahkan periksa kembali password yang anda masukkan.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
            etPass.setText(null);
            etKonfPass.setText(null);
        } else {
            final ProgressDialog pDialog = new ProgressDialog(DaftraAkunActivity.this);
            pDialog.setCancelable(false);
            pDialog.setMessage("Tunggu Sebentar...");
            pDialog.show();
            if (conn.isConnected()) {
                String url = URL.daftarUser;
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                try {
                    params.put("nm_lengkap", etNmLengkap.getText().toString().trim());
                    params.put("jk", spJnsKelamin.getSelectedItem().toString().trim());
                    params.put("email", etEmail.getText().toString().trim());
                    params.put("no_hp", cppNoHp.getSelectedCountryCodeWithPlus()+etNoHp.getText().toString().trim());
                    params.put("username", etUsername.getText().toString().trim());
                    params.put("pass", etKonfPass.getText().toString().trim());
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("error", String.valueOf(e instanceof Exception));
                }

                client.post(url, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.e("status success", String.valueOf(statusCode));
                        try {
                            JSONObject datas = new JSONObject(new String(responseBody));
                            Log.e("loginUSer", datas.toString());
                            if (datas.getInt("response") == 1 && statusCode == 200){
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(DaftraAkunActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_check_green_24dp);
                                dialog.setTitle("Sukses");
                                dialog.setMessage(datas.getString("message"));
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(DaftraAkunActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.setNegativeButton("Tetap disini", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int which) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        etNmLengkap.setText(null);
                        spJnsKelamin.setSelection(0);
                        etEmail.setText(null);
                        cppNoHp.setCountryForNameCode("ID");
                        etNoHp.setText(null);
                        etUsername.setText(null);
                        etPass.setText(null);
                        etKonfPass.setText(null);
                        pDialog.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("status error", String.valueOf(statusCode));
                        Log.e("error", error.toString());
                        if (statusCode == 400) {
                            try {
                                JSONObject datas = new JSONObject(new String(responseBody));
                                Log.e("loginUSer", datas.toString());
                                if (datas.getInt("response") == 0){
                                    Log.e("status", String.valueOf(statusCode));
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(DaftraAkunActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage(datas.getString("message"));
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(DaftraAkunActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(DaftraAkunActivity.this);
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_alert);
                            dialog.setTitle("Kesalahan");
                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            bsdConn.show();
                        }
                        etNmLengkap.setText(null);
                        spJnsKelamin.setSelection(0);
                        etEmail.setText(null);
                        cppNoHp.setCountryForNameCode("ID");
                        etNoHp.setText(null);
                        etUsername.setText(null);
                        etPass.setText(null);
                        etKonfPass.setText(null);
                        pDialog.dismiss();
                    }

                });
            } else {
                bsdConn.show();
                pDialog.dismiss();
            }
        }
    }

    @OnClick(R.id.ll_daftar_akun)
    public void pindahFiturLogin(){
        Intent intent = new Intent(DaftraAkunActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DaftraAkunActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
