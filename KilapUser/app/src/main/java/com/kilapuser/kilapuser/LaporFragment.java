package com.kilapuser.kilapuser;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kilapuser.kilapuser.LaporBencana.HistoryBencanaActivity;
import com.kilapuser.kilapuser.LaporBencana.InputBencanaActivity;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.DatabaseHelper;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class LaporFragment extends Fragment {
    public LaporFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.tv_lapor)
    TextView tvLapor;
    @BindView(R.id.tv_history)
    TextView tvHistory;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;
    DatabaseHelper databaseHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lapor, container, false);
        ButterKnife.bind(this, view);
        conn = new ConnectionDetector(getActivity());
        //modal koneksi
        init_modal_bd_dialog();

        databaseHelper = new DatabaseHelper(getActivity());

        tvLapor.setText(Html.fromHtml("<font>LAPORKAN<br>BENCANA<font>"));
        tvHistory.setText(Html.fromHtml("<font>HISTORY<br>BENCANA<font>"));

        ((MainActivity)getActivity()).tvTitleToolbar.setText("LAPOR");
        return view;
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(getActivity());
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    @OnClick(R.id.cv_lapor)
    public void inputLapor() {
        boolean hapus = databaseHelper.deleteDummyBencana();
        if (hapus){
            Log.e("hapus bencana", "sukses");
        }
        final SQLiteDatabase database = databaseHelper.getReadableDatabase();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Tunggu Sebentar...");
        pDialog.show();

        if (conn.isConnected()) {
            String url = URL.getMasterBencana;
            AsyncHttpClient client = new AsyncHttpClient();

            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    boolean deleteKat = databaseHelper.deleteKatBencana();
                    boolean deleteJns = databaseHelper.deleteJnsBencana();

                    if (deleteKat && deleteJns) {
                        Log.e("delete master", "master bencana berhasil dihapus");
                    } else {
                        Log.e("delete master", "master bencana gagal dihapus");
                    }
                    try {
                        String idKatBencana, katBencana, idJnsBencana, idKjBencana, jnsBencana;
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("getMasterBencana", datas.toString());
                        if (datas.getInt("response") == 1 && statusCode == 200){
                            JSONArray getMaster = datas.getJSONArray("data");
                            for (int i = 0 ;i < getMaster.length(); i++){
                                JSONObject x = getMaster.getJSONObject(i);
                                idKatBencana = x.getString("id_kat_bencana");
                                katBencana = x.getString("kat_bencana");
                                idJnsBencana = x.getString("id_jns_bencana");
                                idKjBencana = x.getString("id_kj_bencana");
                                jnsBencana = x.getString("jns_bencana");
                                Log.e("master", idKatBencana+" "+katBencana+" "+idJnsBencana+" "+jnsBencana);
                                boolean insertKat = databaseHelper.insertKatBencana(idKatBencana, katBencana);
                                boolean insertJns = databaseHelper.insertJnsBencana(idJnsBencana, idKjBencana, jnsBencana);

                                if (insertKat && insertJns) {
                                    Log.e("insert master", "master bencana berhasil ditambahkan");
                                } else {
                                    Log.e("insert master", "master bencana gagal ditambahkan");
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(getActivity(), InputBencanaActivity.class);
                    startActivity(intent);
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Cursor cursorKat = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_KAT_BENCANA, null);
                    Cursor cursorJns = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_JNS_BENCANA, null);
                    if (cursorKat.getCount()>0 && cursorJns.getCount()>0) {
                        Intent intent = new Intent(getActivity(), InputBencanaActivity.class);
                        startActivity(intent);
                    } else {
                        if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_alert);
                            dialog.setTitle("Kesalahan");
                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            bsdConn.show();
                        }
                    }
                    pDialog.dismiss();
                }
            });
        } else {
            Cursor cursorKat = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_KAT_BENCANA, null);
            Cursor cursorJns = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_JNS_BENCANA, null);
            if (cursorKat.getCount()>0 && cursorJns.getCount()>0) {
                Intent intent = new Intent(getActivity(), InputBencanaActivity.class);
                startActivity(intent);
            } else {
                bsdConn.show();
            }
            pDialog.dismiss();
        }

    }

    @OnClick(R.id.cv_history_lapor)
    public void historyLapor() {
        Intent intent = new Intent(getActivity(), HistoryBencanaActivity.class);
        startActivity(intent);
    }

}
