package com.kilapuser.kilapuser.LaporBencana;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kilapuser.kilapuser.R;
import com.kilapuser.kilapuser.ServerSide.DatabaseHelper;
import com.kilapuser.kilapuser.ServerSide.GpsTracker;
import com.kilapuser.kilapuser.ServerSide.PermissionUtils;
import com.kilapuser.kilapuser.ServerSide.UserSession;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class KoordinatBencanaActivity extends FragmentActivity implements OnMapReadyCallback {
    @BindView(R.id.tv_lokasi)
    TextView tvLokasi;
    @BindView(R.id.pb_loading_lokasi)
    ProgressBar pbLoadingLokasi;
    @BindView(R.id.rl_set_lokasi)
    RelativeLayout rlSetLokasi;

    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    String kota, provinsi;
    double lat, lng;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_koordinat_bencana);
        ButterKnife.bind(this);

        databaseHelper = new DatabaseHelper(KoordinatBencanaActivity.this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        configureCameraIdle();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void configureCameraIdle() {
        tvLokasi.setText("Loading...");
        pbLoadingLokasi.setVisibility(View.VISIBLE);
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng latLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(KoordinatBencanaActivity.this);

                try {
                    String lokasi;
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

                    if (addressList != null && addressList.size() > 0) {
//                        lokasi = addressList.get(0).getAddressLine(0);
                        tvLokasi.setText(addressList.get(0).getAddressLine(0));
                        provinsi = addressList.get(0).getAdminArea();
                        kota = addressList.get(0).getSubAdminArea();
                        lat = addressList.get(0).getLatitude();
                        lng = addressList.get(0).getLongitude();
                        Log.e("admin area", addressList.get(0).getAdminArea()); // nama provinsinya
//                        Log.e("sub admin area", addressList.get(0).getSubAdminArea()); //nama kotanya

//                        if (addressList.get(0).getAdminArea().equals("Jawa Barat") && addressList.get(0).getSubAdminArea().equals("Kabupaten Indramayu")){
//
//                        }
                        pbLoadingLokasi.setVisibility(View.GONE);
                    } else {
                        tvLokasi.setText("Loading...");
                        pbLoadingLokasi.setVisibility(View.VISIBLE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }

//    protected synchronized void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .addApi(Places.GEO_DATA_API)
//                .build();
//    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(onCameraIdleListener);
        enableMyLocation();

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void enableMyLocation() {
        GpsTracker gpsTracker= new GpsTracker(getApplicationContext());
        double lat, lng;
        lat = gpsTracker.getLatitude();
        lng = gpsTracker.getLongitude();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(KoordinatBencanaActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
//            if (!(placeIdJemput == null)){
//                CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(latJemputSR, lngJemputSR));
//                CameraUpdate zoom= CameraUpdateFactory.zoomTo(18);
//                mMap.moveCamera(center);
//                mMap.animateCamera(zoom);
//            } else {
//                CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(latMyLokasiJemput, lngMyLokasiJemput));
//                CameraUpdate zoom= CameraUpdateFactory.zoomTo(18);
//                mMap.moveCamera(center);
//                mMap.animateCamera(zoom);
//            }
            CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(lat, lng));
            CameraUpdate zoom= CameraUpdateFactory.zoomTo(16);
            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
//            LatLng myLocation = new LatLng(latitude, longitude);
//            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//            mMap.setPadding(0,0,0,500);
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16.0f));

        }
    }

    @OnClick(R.id.rl_set_lokasi)
    public void setLokasi() {
        if (tvLokasi.getText().toString().isEmpty() || tvLokasi.getText().toString().equals("Loading...") || (lat == 0 && lng == 0)) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(KoordinatBencanaActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Peringatan");
            dialog.setMessage("Pilih lokasi anda dengan benar.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        } else if (!(kota.equals("Kabupaten Indramayu") && provinsi.equals("Jawa Barat"))) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(KoordinatBencanaActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Peringatan");
            dialog.setMessage("Pelaporan digunakan khusus di Kabupaten Indramayu - Jawa Barat");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        } else {
            SQLiteDatabase database = databaseHelper.getReadableDatabase();
            Cursor cursor = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_DUMMY_BENCANA+" WHERE "+DatabaseHelper.KEY_ID_AKUN+" = ?", new String[] {new UserSession(getApplicationContext()).getIdAkun()});
            if (cursor.getCount()>0){
                boolean update = databaseHelper.updateLokasiDummyBencana(new UserSession(getApplicationContext()).getIdAkun(),
                        tvLokasi.getText().toString().trim(), lat, lng);
                if (update) {
                    Log.e("update lokasi", "sukses");
                }
            } else {
                boolean insert = databaseHelper.insertLokasiDummyBencana(new UserSession(getApplicationContext()).getIdAkun(),
                        tvLokasi.getText().toString().trim(), lat, lng);
                if (insert){
                    Log.e("insert lokasi", "sukses");
                }
            }

            finish();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
