package com.kilapuser.kilapuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BantuanFragment extends Fragment {

    public BantuanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bantuan, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity)getActivity()).tvTitleToolbar.setText("BANTUAN");
        return view;
    }

    @OnClick(R.id.rl_berita)
    public void berita(){
        Intent intent = new Intent(getActivity(), BeritaBActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_lapor)
    public void lapor() {
        Intent intent = new Intent(getActivity(), LaporBActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_akun)
    public void akun() {
        Intent intent = new Intent(getActivity(), AkunBActivity.class);
        startActivity(intent);
    }
}
