package com.kilapuser.kilapuser.ServerSide;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    SQLiteDatabase db;
    ContentResolver contentResolver;

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "DB_KILLAP";

    // Table Names
    public static final String TABLE_KAT_BENCANA = "TB_KAT_BENCANA";
    public static final String TABLE_DUMMY_BENCANA = "TB_DUMMY_LAPOR";
    public static final String TABLE_JNS_BENCANA = "TB_JNS_BENCANA";

    // Common column names
    public static final String KEY_ID_KAT_BENCANA = "ID_KAT_BENCANA";
    public static final String KEY_ID_JNS_BENCANA = "ID_JNS_BENCANA";
    public static final String KEY_ID_AKUN = "ID_AKUN";
//    public static final String KEY_ID_BENCANA = "ID_BENCANA";
    public static final String KEY_CREATED_AT = "TDATE";

    // KAT_BENCANA Table - column names
    public static final String KEY_POS_KAT_BENCANA = "POS_KAT_BENCANA";
    public static final String KEY_KAT_BENCANA = "KAT_BENCANA";

    // JNS_BENCANA Table - column names
    public static final String KEY_POS_JNS_BENCANA = "POS_JNS_BENCANA";
    public static final String KEY_JNS_BENCANA = "JNS_BENCANA";

    // DUMMY_BENCANA - column names
    public static final String KEY_KET = "KET";
    public static final String KEY_ALAMAT = "ALAMAT";
    public static final String KEY_LAT = "LAT";
    public static final String KEY_LNG = "LNG";
    public static final String KEY_IMAGE = "IMAGE";
    public static final String KEY_TGL_BENCANA = "TGL_BENCANA";
    public static final String KEY_TGL_PELAPORAN = "TGL_PELAPORAN";

    // KAT_BENCANA table create statement
    private static final String CREATE_TABLE_KAT_BENCANA = "CREATE TABLE "
            + TABLE_KAT_BENCANA + "(" + KEY_ID_KAT_BENCANA + " TEXT PRIMARY KEY,"
            + KEY_KAT_BENCANA + " TEXT NOT NULL,"
            + KEY_CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";

    // JNS_BENCANA table create statement
    private static final String CREATE_TABLE_JNS_BENCANA = "CREATE TABLE "
            + TABLE_JNS_BENCANA + "(" + KEY_ID_JNS_BENCANA + " TEXT PRIMARY KEY,"
            + KEY_ID_KAT_BENCANA + " TEXT NOT NULL,"
            + KEY_JNS_BENCANA + " TEXT NOT NULL,"
            + KEY_CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";

    // DUMMY BENCANA table create statement
    private static final String CREATE_TABLE_DUMMY_BENCANA = "CREATE TABLE "
            + TABLE_DUMMY_BENCANA + "(" + KEY_ID_AKUN + " TEXT PRIMARY KEY,"
            + KEY_ID_KAT_BENCANA + " TEXT,"
            + KEY_ID_JNS_BENCANA + " TEXT,"
            + KEY_POS_KAT_BENCANA + " INT,"
            + KEY_KAT_BENCANA + " TEXT,"
            + KEY_POS_JNS_BENCANA + " INT,"
            + KEY_JNS_BENCANA + " TEXT,"
            + KEY_KET + " TEXT,"
            + KEY_ALAMAT + " TEXT,"
            + KEY_LAT + " DOUBLE,"
            + KEY_LNG + " DOUBLE,"
            + KEY_IMAGE + " BLOB,"
            + KEY_TGL_BENCANA + " DATETIME,"
            + KEY_TGL_PELAPORAN + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        contentResolver = context.getContentResolver();

        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_KAT_BENCANA);
        db.execSQL(CREATE_TABLE_JNS_BENCANA);
        db.execSQL(CREATE_TABLE_DUMMY_BENCANA);
//        db.execSQL(CREATE_TABLE_LOG_PENGIRIMAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KAT_BENCANA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JNS_BENCANA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DUMMY_BENCANA);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG_PENGIRIMAN);
        onCreate(db);
    }

    //method insert kat bencana
    public boolean insertKatBencana(String idKatBencana, String katBencana){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID_KAT_BENCANA, idKatBencana);
        c.put(KEY_KAT_BENCANA, katBencana);

        db.insert(TABLE_KAT_BENCANA, null, c);
        return true;
    }

    //method insert jns bencana
    public boolean insertJnsBencana(String idJnsBencana, String idKatBencana, String jnsBencana){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID_JNS_BENCANA, idJnsBencana);
        c.put(KEY_ID_KAT_BENCANA, idKatBencana);
        c.put(KEY_JNS_BENCANA, jnsBencana);

        db.insert(TABLE_JNS_BENCANA, null, c);
        return true;
    }

    //method insert master
//    public boolean insertMaster(String companyId, String unitcode, String unitname, String station,
//                                String unit, String typecode, String stdParam, String upperLimit,
//                                String lowerLimit, String uom, String nom, String active, String qrCode,
//                                String millcode) {
//        Log.e("Millcode gw", station+" "+millcode);
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues c = new ContentValues();
//        c.put(KEY_COMPANY_ID, companyId);
//        c.put(KEY_UNITCODE, unitcode);
//        c.put(KEY_UNITNAME, unitname);
//        c.put(KEY_STATION, station);
//        c.put(KEY_UNIT, unit);
//        c.put(KEY_TYPECODE, typecode);
//        c.put(KEY_STD_PARAM, stdParam);
//        c.put(KEY_UPPER_LIMIT, upperLimit);
//        c.put(KEY_LOWER_LIMIT, lowerLimit);
//        c.put(KEY_UOM, uom);
//        c.put(KEY_NOM, nom);
//        c.put(KEY_ACTIVE, active);
//        c.put(KEY_QR_CODE, qrCode);
//        c.put(KEY_MILLCODE, millcode);
//
//        db.insert(TABLE_MASTER, null, c);
//        return true;
//    }

    //method insert dummy bencana
    public boolean insertDummyBencana(String idAkun, String idKatBencana, String idJnsBencana,
                                      int posKat, String kat, int posJns, String jns, String ket, String alamat, double lat,
                                      double lng, byte[] image, String tglBencana) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID_AKUN, idAkun);
        c.put(KEY_ID_KAT_BENCANA, idKatBencana);
        c.put(KEY_ID_JNS_BENCANA, idJnsBencana);
        c.put(KEY_POS_KAT_BENCANA, posKat);
        c.put(KEY_KAT_BENCANA, kat);
        c.put(KEY_POS_JNS_BENCANA, posJns);
        c.put(KEY_JNS_BENCANA, jns);
        c.put(KEY_KET, ket);
        c.put(KEY_ALAMAT, alamat);
        c.put(KEY_LAT, lat);
        c.put(KEY_LNG, lng);
        c.put(KEY_IMAGE, image);
        c.put(KEY_TGL_BENCANA, tglBencana);

        db.insert(TABLE_DUMMY_BENCANA, null, c);
        return true;
    }

    //method update
//    public boolean updatetMaster(String id, String companyId, String unitcode, String unitname, String station,
//                                String unit, String typecode, String stdParam, String upperLimit,
//                                String lowerLimit, String uom, String nom, String active, String qrCode,
//                                String millcode) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues c = new ContentValues();
//        c.put(KEY_ID, id);
//        c.put(KEY_COMPANY_ID, companyId);
//        c.put(KEY_UNITCODE, unitcode);
//        c.put(KEY_UNITNAME, unitname);
//        c.put(KEY_STATION, station);
//        c.put(KEY_UNIT, unit);
//        c.put(KEY_TYPECODE, typecode);
//        c.put(KEY_STD_PARAM, stdParam);
//        c.put(KEY_UPPER_LIMIT, upperLimit);
//        c.put(KEY_LOWER_LIMIT, lowerLimit);
//        c.put(KEY_UOM, nom);
//        c.put(KEY_NOM, nom);
//        c.put(KEY_ACTIVE, active);
//        c.put(KEY_QR_CODE, qrCode);
//        c.put(KEY_MILLCODE, millcode);
//
//        db.update(TABLE_MASTER, c, "ID = ?", new String[]{id});
//        return true;
//    }

    //method update mesin
    public boolean updateDummyBencana(String idAkun, String idKatBencana, String idJnsBencana,
                                      int posKat, int posJns, String ket, String alamat, double lat,
                                      double lng, byte[] image, String tglBencana) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID_KAT_BENCANA, idKatBencana);
        c.put(KEY_ID_JNS_BENCANA, idJnsBencana);
        c.put(KEY_POS_KAT_BENCANA, posKat);
        c.put(KEY_POS_JNS_BENCANA, posJns);
        c.put(KEY_KET, ket);
        c.put(KEY_ALAMAT, alamat);
        c.put(KEY_LAT, lat);
        c.put(KEY_LNG, lng);
        c.put(KEY_IMAGE, image);
        c.put(KEY_TGL_BENCANA, tglBencana);

        db.update(TABLE_DUMMY_BENCANA, c, "ID_AKUN = ?", new String[]{idAkun});
        return true;
    }

    //method insert lokasi
    public boolean insertLokasiDummyBencana(String idAkun, String alamat, double lat,
                                            double lng) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID_AKUN, idAkun);
        c.put(KEY_ALAMAT, alamat);
        c.put(KEY_LAT, lat);
        c.put(KEY_LNG, lng);

        db.insert(TABLE_DUMMY_BENCANA, null, c);
        return true;
    }

    //method update lokasi
    public boolean updateLokasiDummyBencana(String idAkun, String alamat, double lat,
                                      double lng) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ALAMAT, alamat);
        c.put(KEY_LAT, lat);
        c.put(KEY_LNG, lng);

        db.update(TABLE_DUMMY_BENCANA, c, "ID_AKUN = ?", new String[]{idAkun});
        return true;
    }

    //method delete kat bencana
    public boolean deleteKatBencana(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_KAT_BENCANA, null, null);
        return true;
    }

    //method delete jns bencana
    public boolean deleteJnsBencana(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_JNS_BENCANA, null, null);
        return true;
    }

    //method delete jns bencana
    public boolean deleteDummyBencana(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DUMMY_BENCANA, null, null);
        return true;
    }

    public List<String> getKategori(){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_KAT_BENCANA};
//        Cursor cursor = db.query(true, TABLE_MASTER, columns, "MILLCODE=?", new String[] { millcode }, null, null, KEY_STATION +" ASC", null);
        Cursor cursor = db.query(true, TABLE_KAT_BENCANA, columns, null, null, null, null, KEY_KAT_BENCANA +" ASC", null);
        Log.e("data master", String.valueOf(cursor.getCount()));

        labels.add("Pilih Kategori Bencana");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getIdKategori(){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_ID_KAT_BENCANA};
//        Cursor cursor = db.query(true, TABLE_MASTER, columns, "MILLCODE=?", new String[] { millcode }, null, null, KEY_STATION +" ASC", null);
        Cursor cursor = db.query(true, TABLE_KAT_BENCANA, columns, null, null, null, null, KEY_KAT_BENCANA +" ASC", null);
//        Log.e("data master", String.valueOf(cursor.getCount()));

        labels.add("Pilih Kategori Bencana");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getJenis(String idKat){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_JNS_BENCANA};
//        Cursor cursor = db.query(true, TABLE_MASTER, columns, "MILLCODE=?", new String[] { millcode }, null, null, KEY_STATION +" ASC", null);
        Cursor cursor = db.query(true, TABLE_JNS_BENCANA, columns, "ID_KAT_BENCANA=?",  new String[] {idKat}, null, null, KEY_JNS_BENCANA +" ASC", null);
//        Log.e("data master", String.valueOf(cursor.getCount()));

        labels.add("Pilih Jenis Bencana");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getIdJenis(String idKat){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_ID_JNS_BENCANA};
//        Cursor cursor = db.query(true, TABLE_MASTER, columns, "MILLCODE=?", new String[] { millcode }, null, null, KEY_STATION +" ASC", null);
        Cursor cursor = db.query(true, TABLE_JNS_BENCANA, columns, "ID_KAT_BENCANA=?",  new String[] {idKat}, null, null, KEY_JNS_BENCANA +" ASC", null);
        Log.e("data master", String.valueOf(cursor.getCount()));

        labels.add("Pilih Jenis Bencana");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }
//
//    public List<String> getAllUnitcode(String millcode, String station){
//        List<String> labels = new ArrayList<String>();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {KEY_UNITCODE};
//        Cursor cursor = db.query(TABLE_MASTER, columns, "STATION=?", new String[] {station}, null, null, KEY_UNITCODE +" ASC");
//        labels.add(0, "Pilih Unitcode");
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()){
//            do {
//                labels.add(cursor.getString(0));
//            } while (cursor.moveToNext());
//        }
//
//        // closing connection
//        cursor.close();
//        db.close();
//
//        //returning labels
//        return labels;
//    }
//
//    public List<String> getVrsion(){
//        List<String> labels = new ArrayList<String>();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {KEY_VERSION};
//        Cursor cursor = db.query(TABLE_VERSION_UPDATE, columns, null, null, null, null, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()){
//            do {
//                labels.add(cursor.getString(1));
//            } while (cursor.moveToNext());
//        }
//
//        // closing connection
//        cursor.close();
//        db.close();
//
//        //returning labels
//        return labels;
//    }
//
//    public List<String> getFilterData(String station, String unitcode, String millcode){
//        List<String> labels = new ArrayList<String>();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        String[] columns = {KEY_UNITCODE};
//        Cursor cursor = db.query(TABLE_MASTER, columns, "STATION=? AND UNITCODE=? AND MILLCODE=?", new String[] { station, unitcode, millcode }, null, null, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()){
//            do {
//                labels.add(cursor.getString(0));
//            } while (cursor.moveToNext());
//        }
//
//        // closing connection
//        cursor.close();
//        db.close();
//
//        //returning labels
//        return labels;
//    }
}
