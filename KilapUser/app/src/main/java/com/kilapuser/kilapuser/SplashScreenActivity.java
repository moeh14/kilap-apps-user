package com.kilapuser.kilapuser;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLException;

import cz.msebera.android.httpclient.Header;

public class SplashScreenActivity extends AppCompatActivity {
    String idProfile, idAkun, nama, jk, noHp, email, foto, alamat, username, pass, level, status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (android.os.Build.VERSION.SDK_INT > 14) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        String url = URL.getDataUser+new UserSession(getApplicationContext()).getIdAkun();
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.e("status success", String.valueOf(statusCode));
                try {
                    JSONObject datas = new JSONObject(new String(responseBody));
                    Log.e("dataUSer", datas.toString());
                    if (datas.getInt("response") == 1 && statusCode == 200){
                        JSONArray profileUser = datas.getJSONArray("data");
                        for (int i = 0; i < profileUser.length(); i++) {
                            JSONObject x = profileUser.getJSONObject(i);
                            idProfile = x.getString("id_profile_user");
                            idAkun = x.getString("id_akun");
                            nama = x.getString("nama");
                            jk = x.getString("jk");
                            noHp = x.getString("no_hp");
                            email = x.getString("email");
                            foto = x.getString("foto");
                            alamat = x.getString("alamat");
                            username = x.getString("username");
                            pass = x.getString("password");
                            level = x.getString("level");
                            status = x.getString("status");
                        }
                        UserSession x = new UserSession(SplashScreenActivity.this);
                        x.setIdProfileUser(idProfile);
                        x.setIdAkun(idAkun);
                        x.setNama(nama);
                        x.setJk(jk);
                        x.setNoHp(noHp);
                        x.setEmail(email);
                        if (foto.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                            x.setFoto(null);
                        } else {
                            x.setFoto(foto);
                        }
                        Log.e("alamat", String.valueOf(alamat.length()));
                        if (alamat.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                            x.setAlamat(null);
                        } else {
                            x.setAlamat(alamat);
                        }
                        x.setUsername(username);
                        x.setPassword(pass);
                        x.setLevel(level);
                        x.setStatus(status);

                        splashScreenDelay();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("status error", String.valueOf(statusCode));
                Log.e("error", error.toString());
                if (statusCode == 400) {
                    try {
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("loginUSer", datas.toString());
                        if (datas.getInt("response") == 0){
                            new UserSession(getApplicationContext()).clearSession();
                            splashScreenDelay();
                        } else {
                            splashScreenDelay();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    splashScreenDelay();
                }
            }
        });
    }

    private void splashScreenDelay() {
        final int welcomeScreenDisplay = 3000; // 3000 = 3 detik
        Thread welcomeThread = new Thread() {
            int wait = 0;
            @Override
            public void run() {
                try {
                    super.run();
                    while (wait < welcomeScreenDisplay) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {
//                    if (new UserSession(getApplicationContext()).getIsLogin()){
//                        OrderPreference o = new OrderPreference(getApplicationContext());
//                        String status = o.getStatus();
//                        if (status.equals("1")){
//                            Intent intent = new Intent(SplashScreenActivity.this, OrderSebelumRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else if (status.equals("2")) {
//                            Intent intent = new Intent(SplashScreenActivity.this, OrderSetelahRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else if (status.equals("3")) {
//                            Intent intent = new Intent(SplashScreenActivity.this, RangkumanRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else {
//                            Intent intent = new Intent(SplashScreenActivity.this, DashboardActivity.class);
//                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        }
//                    } else {
//                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
//                        startActivity(intent);
//                    }
                    UserSession x = new UserSession(SplashScreenActivity.this);
                    if (x.getIsLogin()) {
                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                    finish();
                }
            }
        };
        welcomeThread.start();
    }
}
