package com.kilapuser.kilapuser;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kilapuser.kilapuser.LaporBencana.DetailHistoryActivity;
import com.kilapuser.kilapuser.Model.ModelBeritaBencana;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterBeritaBencana extends RecyclerView.Adapter<AdapterBeritaBencana.HolderItem> {
    private Context context;
    private List<ModelBeritaBencana> datas;
    public AdapterBeritaBencana(Context context) {
        this.context = context;
    }
    @NonNull
    @Override
    public HolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_berita_bencana, null);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderItem holderItem, int i) {
        final ModelBeritaBencana x = datas.get(i);
        holderItem.tvJns.setText(x.getJns_bencana());
        holderItem.tvKat.setText(x.getKat_bencana());
        Picasso.with(context).load(URL.rootImage + x.getImage())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holderItem.ivBencana, new Callback() {
                    @Override
                    public void onSuccess() {
//                        holder.loadingImage.setVisibility(View.GONE);
                        holderItem.pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        holder.loadingImage.setVisibility(View.GONE);
                        holderItem.pbImage.setVisibility(View.GONE);
                        holderItem.ivBencana.setImageResource(R.drawable.ic_no_image);
                    }
                });
        holderItem.tvKet.setText(x.getKet());
        holderItem.tvLokasi.setText("Lokasi : "+x.getAlamat());
        holderItem.tvTglBencana.setText("Tanggal Bencana : "+x.getTgl_bencana());
        holderItem.tvTglLapor.setText(x.getTgl_pelaporan());
        holderItem.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailBeritaActivity.class);
                intent.putExtra("id", x.getId_bencana());
                intent.putExtra("kat", x.getKat_bencana());
                intent.putExtra("jns", x.getJns_bencana());
                intent.putExtra("nama_user", x.getNama_user());
                intent.putExtra("alamat", x.getAlamat());
                intent.putExtra("lat", x.getLat());
                intent.putExtra("lng", x.getLng());
                intent.putExtra("ket", x.getKet());
                intent.putExtra("image", x.getImage());
                intent.putExtra("tgl_bencana", x.getTgl_bencana());
                intent.putExtra("tgl_lapor", x.getTgl_pelaporan());
                intent.putExtra("nama", x.getNama_dinas());
                intent.putExtra("status", x.getStatus_survey());
                intent.putExtra("tgl_hasil", x.getTgl_survey());
                intent.putExtra("ket_hasil", x.getKet_survey());
                intent.putExtra("image_hasil", x.getImage_survey());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setDatas(List<ModelBeritaBencana> datas) {
        this.datas = datas;
    }

    public List<ModelBeritaBencana> getDatas() {
        return datas;
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        @BindView(R.id.tv_jns)
        TextView tvJns;
        @BindView(R.id.tv_kat)
        TextView tvKat;
        @BindView(R.id.iv_bencana)
        ImageView ivBencana;
        @BindView(R.id.pb_load_image)
        ProgressBar pbImage;
        @BindView(R.id.tv_ket)
        TextView tvKet;
        @BindView(R.id.tv_lokasi)
        TextView tvLokasi;
        @BindView(R.id.tv_tgl_bencana)
        TextView tvTglBencana;
        @BindView(R.id.tv_tgl_lapor)
        TextView tvTglLapor;

        public HolderItem(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
