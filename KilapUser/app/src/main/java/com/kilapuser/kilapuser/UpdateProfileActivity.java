package com.kilapuser.kilapuser;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.hbb20.CountryCodePicker;
import com.kilapuser.kilapuser.LaporBencana.InputBencanaActivity;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;
import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateProfileActivity extends AppCompatActivity {
    @BindView(R.id.civ_image)
    CircleImageView civImage;
    @BindView(R.id.pb_image)
    ProgressBar pbImage;
    @BindView(R.id.et_nm_lengkap)
    EditText etNmLengkap;
    @BindView(R.id.sp_jns_kelamin)
    Spinner spJnsKelamin;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_no_hp)
    EditText etNoHp;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_alamat)
    EditText etALamat;

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private Bitmap thumbnail;
    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("UPDATE PROFILE");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //modal koneksi
        init_modal_bd_dialog();

        conn = new ConnectionDetector(getApplicationContext());

        spinnerDataJnsKelamin();

        UserSession x = new UserSession(getApplicationContext());
        etNmLengkap.setText(x.getNama());
        if (new UserSession(getApplicationContext()).getJk().equals("Laki - laki")) {
            spJnsKelamin.setSelection(1);
        } else {
            spJnsKelamin.setSelection(2);
        }
        etEmail.setText(x.getEmail());
        etNoHp.setText(x.getNoHp());
        etUsername.setText(x.getUsername());
        if (x.getAlamat().isEmpty() || x.getAlamat().equals(null)){
            etALamat.setText(null);
        } else {
            etALamat.setText(x.getAlamat());
        }

        Picasso.with(getApplicationContext()).load(URL.rootImage + x.getFoto())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(civImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
                        pbImage.setVisibility(View.GONE);
                        civImage.setImageResource(R.drawable.avatar);
                    }
                });
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    private void spinnerDataJnsKelamin() {
        List<String> labels = new ArrayList<String>();
        labels.add(0, "Pilih jenis kelamin");
        labels.add(1, "Laki - laki");
        labels.add(2, "Perempuan");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_list, labels);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spJnsKelamin.setAdapter(dataAdapter);
    }

    @OnClick(R.id.btn_update)
    public void update() {
        if (etNmLengkap.getText().toString().isEmpty() || spJnsKelamin.getSelectedItem().toString().equals("Pilih jenis kelamin") || etEmail.getText().toString().isEmpty() ||
                etNoHp.getText().toString().isEmpty() || etUsername.getText().toString().isEmpty() || etALamat.getText().toString().isEmpty()) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Silahkan lengkapi form profil untuk melanjutkan.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        } else {
            final ProgressDialog pDialog = new ProgressDialog(UpdateProfileActivity.this);
            pDialog.setCancelable(false);
            pDialog.setMessage("Tunggu Sebentar...");
            pDialog.show();
            if (conn.isConnected()) {
                String url = URL.updateProfileUser;
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                try {
                    params.put("id_akun", new UserSession(getApplicationContext()).getIdAkun());
                    params.put("nm_lengkap", etNmLengkap.getText().toString().trim());
                    params.put("jk", spJnsKelamin.getSelectedItem().toString().trim());
                    params.put("email", etEmail.getText().toString().trim());
                    params.put("no_hp", etNoHp.getText().toString().trim());
                    params.put("username", etUsername.getText().toString().trim());
                    params.put("alamat", etALamat.getText().toString().trim());
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("error", String.valueOf(e instanceof Exception));
                }

                client.post(url, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.e("status success", String.valueOf(statusCode));
                        try {
                            JSONObject datas = new JSONObject(new String(responseBody));
                            Log.e("loginUSer", datas.toString());
                            if (datas.getInt("response") == 1 && statusCode == 200){
                                Log.e("nama", etNmLengkap.getText().toString().trim());
                                UserSession x = new UserSession(getApplicationContext());
                                x.setNama(etNmLengkap.getText().toString().trim());
                                x.setJk(spJnsKelamin.getSelectedItem().toString().trim());
                                x.setEmail(etEmail.getText().toString().trim());
                                x.setNoHp(etNoHp.getText().toString().trim());
                                x.setUsername(etUsername.getText().toString().trim());
                                x.setAlamat(etALamat.getText().toString().trim());
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_check_green_24dp);
                                dialog.setTitle("Sukses");
                                dialog.setMessage(datas.getString("message"));
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.setNegativeButton("Tetap disini", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int which) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        etNmLengkap.setText(null);
                        spJnsKelamin.setSelection(0);
                        etEmail.setText(null);
                        etNoHp.setText(null);
                        etUsername.setText(null);
                        etALamat.setText(null);
                        pDialog.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("status error", String.valueOf(statusCode));
                        Log.e("error", error.toString());
                        if (statusCode == 400) {
                            try {
                                JSONObject datas = new JSONObject(new String(responseBody));
                                Log.e("loginUSer", datas.toString());
                                if (datas.getInt("response") == 0){
                                    Log.e("status", String.valueOf(statusCode));
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage(datas.getString("message"));
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_alert);
                            dialog.setTitle("Kesalahan");
                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            bsdConn.show();
                        }
                        pDialog.dismiss();
                    }

                });
            } else {
                bsdConn.show();
                pDialog.dismiss();
            }
        }
    }

    @OnClick(R.id.tv_ubah_foto)
    public void ubahFoto(){
        new MaterialDialog.Builder(this)
                .title(R.string.uploadImages)
                .items(R.array.uploadImages1)
                .itemsIds(R.array.itemIds1)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which){
                            case 0:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE_PHOTO);
                                break;
                        }
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAPTURE_PHOTO){
            if(resultCode == RESULT_OK) {
                onCaptureImageResult1(data);
                Log.e("data", String.valueOf(data));
            }
        }
    }

    private void onCaptureImageResult1(final Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        civImage.setImageBitmap(thumbnail);
        final ProgressDialog pDialog = new ProgressDialog(UpdateProfileActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Tunggu Sebentar...");
        pDialog.show();
        if (conn.isConnected()) {
            String url = URL.updateImgUser;
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            try {
                params.put("id_akun", new UserSession(getApplicationContext()).getIdAkun());
                params.put("attachment", storeImage(thumbnail));
            } catch (Exception e){
                e.printStackTrace();
                Log.e("error", String.valueOf(e instanceof Exception));
            }

            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.e("status success", String.valueOf(statusCode));
                    try {
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("loginUSer", datas.toString());
                        if (datas.getInt("response") == 1 && statusCode == 200){
                            String img = datas.getString("img");
//                                Log.e("nama", etNmLengkap.getText().toString().trim());
                                UserSession x = new UserSession(getApplicationContext());
                                x.setFoto(img);
//                                x.setNama(etNmLengkap.getText().toString().trim());
//                                x.setJk(spJnsKelamin.getSelectedItem().toString().trim());
//                                x.setEmail(etEmail.getText().toString().trim());
//                                x.setNoHp(etNoHp.getText().toString().trim());
//                                x.setUsername(etUsername.getText().toString().trim());
//                                x.setAlamat(etALamat.getText().toString().trim());
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_check_green_24dp);
                            dialog.setTitle("Sukses");
                            dialog.setMessage(datas.getString("message"));
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.setNegativeButton("Tetap disini", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.e("status error", String.valueOf(statusCode));
                    Log.e("error", error.toString());
                    if (statusCode == 400) {
                        try {
                            JSONObject datas = new JSONObject(new String(responseBody));
                            Log.e("loginUSer", datas.toString());
                            if (datas.getInt("response") == 0){
                                Log.e("status", String.valueOf(statusCode));
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_alert);
                                dialog.setTitle("Kesalahan");
                                dialog.setMessage(datas.getString("message"));
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();
                            } else {
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_alert);
                                dialog.setTitle("Kesalahan");
                                dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                        final AlertDialog.Builder dialog = new AlertDialog.Builder(UpdateProfileActivity.this);
                        dialog.setCancelable(false);
                        dialog.setIcon(R.drawable.ic_alert);
                        dialog.setTitle("Kesalahan");
                        dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        bsdConn.show();
                    }
                    pDialog.dismiss();
                }

            });
        } else {
            bsdConn.show();
            pDialog.dismiss();
        }
    }

    private File storeImage(Bitmap bitmap) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("file",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("error file", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("error file1", "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }
}
