package com.kilapuser.kilapuser.LaporBencana;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kilapuser.kilapuser.DaftraAkunActivity;
import com.kilapuser.kilapuser.LoginActivity;
import com.kilapuser.kilapuser.R;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.DatabaseHelper;
import com.kilapuser.kilapuser.ServerSide.MySingleton;
import com.kilapuser.kilapuser.ServerSide.PermissionUtils;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.kilapuser.kilapuser.ServerSide.VolleyHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;
import cz.msebera.android.httpclient.entity.mime.content.ContentBody;
import cz.msebera.android.httpclient.entity.mime.content.InputStreamBody;

public class InputBencanaActivity extends FragmentActivity implements OnMapReadyCallback {
    @BindView(R.id.sp_kat_bencana)
    Spinner spKatBencana;
    @BindView(R.id.sp_jns_bencana)
    Spinner spJnsBencana;
    @BindView(R.id.et_pelapor)
    EditText etPelapor;
    @BindView(R.id.tv_pilih_map)
    TextView tvPilihMap;
    @BindView(R.id.ll_map)
    LinearLayout llMap;
    @BindView(R.id.ll_pilih_lokasi)
    LinearLayout llPilihLokasi;
    @BindView(R.id.ll_alamat)
    LinearLayout llAlamat;
    @BindView(R.id.et_alamat_bencana)
    EditText etAlamatBencana;
    @BindView(R.id.et_ket_bencana)
    EditText etKetBencana;
    @BindView(R.id.iv_bencana)
    ImageView ivBencana;
    @BindView(R.id.tv_upload)
    TextView tvUpload;
    @BindView(R.id.et_tgl_bencana)
    EditText etTglBencana;
    @BindView(R.id.et_wkt_bencana)
    EditText etWktBencana;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    private SimpleDateFormat dateFormat, dateFormatFix;
    Locale locale;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    private DatabaseHelper databaseHelper;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private double latitude, longitude;
    private String idKat, idJns, tglFixBencana, wktFixBencana;
    List<String> labelsIdKat,labelsKat, labelsIdJns, labelsJns;
    ArrayAdapter<String> dataAdapterJenis;
    private Bitmap thumbnail;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_bencana);
        ButterKnife.bind(this);

        conn = new ConnectionDetector(InputBencanaActivity.this);
        databaseHelper = new DatabaseHelper(InputBencanaActivity.this);

        //modal koneksi
        init_modal_bd_dialog();

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("INPUT LAPOR");
        tvUpload.setText(Html.fromHtml("<font>Klik untuk upload<br>gambar bencana<font>"));

        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage("Apakah anda ingin membatalkan inputan ?");
                dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        boolean hapus = databaseHelper.deleteDummyBencana();
                        if (hapus){
                            Log.e("hapus bencana", "sukses");
                        }
                        dialogInterface.dismiss();
                        finish();
                    }
                });
                dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        locale = new Locale("in");
        dateFormat = new SimpleDateFormat("dd - MMMM - yyyy", locale);
        dateFormatFix = new SimpleDateFormat("yyyy-MM-dd", locale);

//        int pIdKat, pPosKat, pKat, pIdJns, pPosJns, pJns, pLat, pLng, pAlamat, pKet, pGambar, pTglWaktu;
//        SQLiteDatabase database = databaseHelper.getReadableDatabase();
//        Cursor cursor = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_DUMMY_BENCANA, null);
//        pIdKat = cursor.getColumnIndex(DatabaseHelper.KEY_ID_KAT_BENCANA);
//        pPosKat = cursor.getColumnIndex(DatabaseHelper.KEY_POS_KAT_BENCANA);
//        pKat = cursor.getColumnIndex(DatabaseHelper.KEY_KAT_BENCANA);
//        pIdJns = cursor.getColumnIndex(DatabaseHelper.KEY_ID_JNS_BENCANA);
//        pPosJns = cursor.getColumnIndex(DatabaseHelper.KEY_POS_JNS_BENCANA);
//        pJns = cursor.getColumnIndex(DatabaseHelper.KEY_JNS_BENCANA);
//        pLat = cursor.getColumnIndex(DatabaseHelper.KEY_LAT);
//        pLng = cursor.getColumnIndex(DatabaseHelper.KEY_LNG);
//        pAlamat = cursor.getColumnIndex(DatabaseHelper.KEY_ALAMAT);
//        pKet = cursor.getColumnIndex(DatabaseHelper.KEY_KET);
//        pGambar = cursor.getColumnIndex(DatabaseHelper.KEY_IMAGE);
//        pTglWaktu = cursor.getColumnIndex(DatabaseHelper.KEY_TGL_BENCANA);

        loadSpinnerKategori();
        int positionKat = spKatBencana.getSelectedItemPosition();
        idKat = labelsIdKat.get(positionKat);
        loadSpinnerJenis(idKat);
        int positionJns = spJnsBencana.getSelectedItemPosition();
        idJns = labelsIdJns.get(positionJns);

//        if (cursor.getCount()>0){
//            cursor.moveToFirst();
//            loadSpinnerKategori();
////            int positionKat = spKatBencana.getSelectedItemPosition();
////            idKat = labelsIdKat.get(positionKat);
//            idKat = cursor.getString(pIdKat);
//            loadSpinnerJenis(idKat);
//
//            Log.e("sql jns", String.valueOf(cursor.getInt(pPosJns)));
//            Log.e("id kat", String.valueOf(spKatBencana.getSelectedItemId()));
//            spKatBencana.setSelection(cursor.getInt(pPosKat));
//            spJnsBencana.setSelection(dataAdapterJenis.getPosition(cursor.getString(pPosJns)));
//            idJns = cursor.getString(pIdJns);
//
//            latitude = cursor.getDouble(pLat);
//            longitude = cursor.getDouble(pLng);
//            etAlamatBencana.setText(cursor.getString(pAlamat));
//            etKetBencana.setText(cursor.getString(pKet));
//            //gambar
//            if (data != null) {
//                data = cursor.getBlob(pGambar);
//                thumbnail = BitmapFactory.decodeByteArray(data, 0, data.length);
//                ivBencana.setImageBitmap(thumbnail);
//            }
//        }

        //spinner kategori
        spKatBencana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idKat = labelsIdKat.get(position);
                loadSpinnerJenis(idKat);
                Log.e("id kat", idKat);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //spinner jenis
        spJnsBencana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idJns = labelsIdJns.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //pelapor
        etPelapor.setText(new UserSession(getApplicationContext()).getNama());

        //latlng dan lokasi
        lokasi();


    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    private void lokasi() {
        int pAlamat, pLat, pLng;
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_DUMMY_BENCANA+" WHERE "+DatabaseHelper.KEY_ID_AKUN+" = ?", new String[] {new UserSession(getApplicationContext()).getIdAkun()});
        pAlamat = cursor.getColumnIndex(DatabaseHelper.KEY_ALAMAT);
        pLat = cursor.getColumnIndex(DatabaseHelper.KEY_LAT);
        pLng = cursor.getColumnIndex(DatabaseHelper.KEY_LNG);
        if (cursor.getCount()>0) {
            cursor.moveToFirst();
            latitude = cursor.getDouble(pLat);
            longitude = cursor.getDouble(pLng);
            etAlamatBencana.setText(cursor.getString(pAlamat));
        }

        if (latitude == 0 && longitude == 0) {
            llAlamat.setVisibility(View.GONE);
            tvPilihMap.setText("Pilih lokasi bencana");
            llMap.setVisibility(View.GONE);

        } else {
            llAlamat.setVisibility(View.VISIBLE);
            tvPilihMap.setText("Ubah lokasi bencana");
            llMap.setVisibility(View.VISIBLE);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @OnClick(R.id.rl_upload_gambar)
    public void uploadGambar(){
        new MaterialDialog.Builder(this)
                .title(R.string.uploadImages)
                .items(R.array.uploadImages)
                .itemsIds(R.array.itemIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which){
                            case 0:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE_PHOTO);
                                break;
                            case 1:
                                ivBencana.setImageDrawable(null);
                                break;
                        }
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAPTURE_PHOTO){
            if(resultCode == RESULT_OK) {
                onCaptureImageResult1(data);
                Log.e("data", String.valueOf(data));
            }
        }
    }

    private void onCaptureImageResult1(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");

        ivBencana.setImageBitmap(thumbnail);
    }

    private void loadSpinnerJenis(String idKat) {
        //spinner dropdown elements
        labelsIdJns = databaseHelper.getIdJenis(idKat);
        labelsJns = databaseHelper.getJenis(idKat);
        Log.e("kategori", labelsIdJns+" - "+labelsJns.toString());

        // Creating adapter for spinner
        dataAdapterJenis = new ArrayAdapter<String>(InputBencanaActivity.this, R.layout.spinner_list, labelsJns);

        // Drop down layout style - list view with radio button
        dataAdapterJenis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spJnsBencana.setAdapter(dataAdapterJenis);
    }

    private void loadSpinnerKategori() {
        //spinner dropdown elements
        labelsKat = databaseHelper.getKategori();
        labelsIdKat = databaseHelper.getIdKategori();
        Log.e("kategori", labelsIdKat+" - "+labelsKat.toString());

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(InputBencanaActivity.this, R.layout.spinner_list, labelsKat);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spKatBencana.setAdapter(dataAdapter);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.clear();

        // Add a marker in Sydney and move the camera
        LatLng lokasiBencana = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(lokasiBencana));
        CameraUpdate center=CameraUpdateFactory.newLatLng(lokasiBencana);
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(11);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(InputBencanaActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(InputBencanaActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(-34, 151));
            CameraUpdate zoom=CameraUpdateFactory.zoomTo(16);
            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
        }
    }

    @OnClick(R.id.et_tgl_bencana)
    public void dateBencana() {
        /**
         * Calendar untuk mendapatkan tanggal sekarang
         */
        Calendar newCalendar = Calendar.getInstance();

        /**
         * Initiate DatePicker dialog
         */
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                etTglBencana.setText(dateFormat.format(newDate.getTime()));
                tglFixBencana = dateFormatFix.format(newDate.getTime());
                Log.e("tgl fix", tglFixBencana);
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        datePickerDialog.show();
    }

    @OnClick(R.id.et_wkt_bencana)
    public void etWktBencana() {
        /**
         * Calendar untuk mendapatkan waktu saat ini
         */
        Calendar calendar = Calendar.getInstance();

        /**
         * Initialize TimePicker Dialog
         */
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                /**
                 * Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                 */
                etWktBencana.setText(hourOfDay+" : "+minute+" WIB");
                wktFixBencana = hourOfDay+":"+minute+":00";
            }
        },
                /**
                 * Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                 */
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                /**
                 * Cek apakah format waktu menggunakan 24-hour format
                 */
                DateFormat.is24HourFormat(this));

        timePickerDialog.show();
    }

    @OnClick(R.id.ll_pilih_lokasi)
    public void pilihLokasi() {
        Intent intent = new Intent(InputBencanaActivity.this, KoordinatBencanaActivity.class);
        startActivity(intent);
    }

    byte[] data;
    @OnClick({R.id.btn_lapor})
    public void laporBencana() {
//        String alamat = etAlamatBencana.getText().toString().trim();
//        Log.e("alamat", alamat);
//        if (new UserSession(getApplicationContext()).getStatus()){
//
//        }
        int status = Integer.parseInt(new UserSession(getApplicationContext()).getStatus());
        Log.e("status", String.valueOf(status));
        if (status == 1){
            if (spKatBencana.getSelectedItemPosition() == 0 || spJnsBencana.getSelectedItemPosition() == 0 ||
                    etPelapor.getText().toString().isEmpty() || latitude == 0 || longitude == 0 || etAlamatBencana.getText().toString().isEmpty() ||
                    etKetBencana.getText().toString().isEmpty() || thumbnail == null || (etTglBencana.getText().toString().isEmpty() &&
                    tglFixBencana.isEmpty()) || (etWktBencana.getText().toString().isEmpty() && wktFixBencana.isEmpty())) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                dialog.setCancelable(false);
                dialog.setIcon(R.drawable.ic_alert);
                dialog.setTitle("Kesalahan");
                dialog.setMessage("Silahkan lengkapi form inputan laporan bencana anda.");
                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialog.show();
            } else {
//            ivBencana.setDrawingCacheEnabled(true);
//            ivBencana.buildDrawingCache();
//            final Bitmap bitmap = ivBencana.getDrawingCache();
//            Log.e("image file", String.valueOf(storeImage(bitmap)));
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
//            data = baos.toByteArray();

                final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("Lapor Bencana");
                dialog.setMessage("Apakah anda yakin dengan data - data laporan yang anda masukkan?");
                dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final ProgressDialog pDialog = new ProgressDialog(InputBencanaActivity.this);
                        pDialog.setCancelable(false);
                        pDialog.setMessage("Tunggu Sebentar...");
                        pDialog.show();
                        if (conn.isConnected()){
                            String url = URL.laporBencana;
                            AsyncHttpClient client= new AsyncHttpClient();
                            RequestParams params = new RequestParams();
                            try {
                                params.put("id_user", new UserSession(getApplicationContext()).getIdAkun());
                                params.put("id_kat_bencana", idKat);
                                params.put("id_jns_bencana", idJns);
                                params.put("ket", etKetBencana.getText().toString().trim());
                                params.put("alamat", etAlamatBencana.getText().toString().trim());
                                params.put("lat", latitude);
                                params.put("lng", longitude);
                                params.put("attachment", storeImage(thumbnail));
                                params.put("tgl_bencana", tglFixBencana+" "+wktFixBencana);
//                            params.setHttpEntityIsRepeatable(true);
//                            params.setUseJsonStreamer(false);
                            } catch (Exception e){
                                e.printStackTrace();
                                Log.e("error", String.valueOf(e instanceof FileNotFoundException));
                            }

                            client.post(url, params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    try {
                                        JSONObject datas = new JSONObject(new String(responseBody));
                                        Log.e("lapor", datas.toString());
                                        if (datas.getInt("response") == 1 && statusCode == 200) {
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                                            dialog.setCancelable(false);
                                            dialog.setIcon(R.drawable.ic_check_green_24dp);
                                            dialog.setTitle("Sukses");
                                            dialog.setMessage(datas.getString("message"));
                                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    finish();
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            dialog.show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    spKatBencana.setSelection(0);
                                    idKat = null;
                                    spJnsBencana.setSelection(0);
                                    idJns = null;
                                    etPelapor.setText(null);
                                    latitude = 0;
                                    longitude = 0;
                                    llAlamat.setVisibility(View.GONE);
                                    tvPilihMap.setText("Pilih lokasi bencana");
                                    llMap.setVisibility(View.GONE);
                                    etAlamatBencana.setText(null);
                                    etKetBencana.setText(null);
                                    ivBencana.setImageDrawable(null);
                                    data = null;
                                    etTglBencana.setText(null);
                                    etWktBencana.setText(null);
                                    tglFixBencana = null;
                                    wktFixBencana = null;
                                    pDialog.dismiss();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Log.e("status", String.valueOf(statusCode));
                                    if (statusCode == 400) {
                                        try {
                                            JSONObject datas = new JSONObject(new String(responseBody));
                                            Log.e("lapor bencana error", datas.toString());
                                            if (datas.getInt("response") == 0) {
                                                final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setIcon(R.drawable.ic_alert);
                                                dialog.setTitle("Kesalahan");
                                                dialog.setMessage(datas.getString("message"));
                                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                            } else {
                                                final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                                                dialog.setCancelable(false);
                                                dialog.setIcon(R.drawable.ic_alert);
                                                dialog.setTitle("Kesalahan");
                                                dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                            }
                                            pDialog.dismiss();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else if(statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException) {
                                        final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setIcon(R.drawable.ic_alert);
                                        dialog.setTitle("Kesalahan");
                                        dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        dialog.show();
                                    } else {
                                        bsdConn.show();
                                    }
                                    pDialog.dismiss();
                                }
                            });
                        } else {
                            bsdConn.show();
                            pDialog.dismiss();
                        }
                        dialogInterface.dismiss();
                    }
                });
                dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


            }
        } else {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Anda tidak dapat melapor bencana karena akun anda telah diblokir karena mengganggu kenyamanan aplikasi.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        }
    }

    private File storeImage(Bitmap bitmap) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("file",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("error file", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("error file1", "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("resume", "jalan");
        lokasi();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("pause", "jalan");
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(InputBencanaActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Apakah anda ingin membatalkan inputan ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean hapus = databaseHelper.deleteDummyBencana();
                if (hapus){
                    Log.e("hapus bencana", "sukses");
                }
                dialogInterface.dismiss();
                finish();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }
}
