package com.kilapuser.kilapuser;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kilapuser.kilapuser.LaporBencana.KoordinatBencanaActivity;
import com.kilapuser.kilapuser.ServerSide.ConnectionDetector;
import com.kilapuser.kilapuser.ServerSide.PermissionUtils;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.kilapuser.kilapuser.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.btn_login_akun)
    Button btnLogin;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //modal koneksi
        init_modal_bd_dialog();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(LoginActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }

        conn = new ConnectionDetector(getApplicationContext());

        tvLogin.setText(Html.fromHtml("<font>Tidak Punya Akun? <b>Buat Akun</b><font>"));

    }

    String idProfile, idAkun, nama, jk, noHp, email, foto, alamat, username, pass, level, status;
    @OnClick(R.id.btn_login_akun)
    public void btnLogin(){
        if (etUsername.getText().toString().isEmpty() && etPassword.getText().toString().isEmpty()) {
            etUsername.setError("Harap diisi");
            etPassword.setError("Harap diisi");
        } else if (etUsername.getText().toString().isEmpty()){
            etUsername.setError("Harap diisi");
        } else if (etPassword.getText().toString().isEmpty()){
            etPassword.setError("Harap diisi");
        } else {
            final ProgressDialog pDialogLogin = new ProgressDialog(LoginActivity.this);
            pDialogLogin.setCancelable(false);
            pDialogLogin.setMessage("Tunggu Sebentar...");
            pDialogLogin.show();
            if (conn.isConnected()) {
                String url = URL.loginUser;
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                try {
                    params.put("username", etUsername.getText().toString().trim());
                    params.put("pass", etPassword.getText().toString().trim());
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("error", String.valueOf(e instanceof Exception));
                }

                client.post(url, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.e("status success", String.valueOf(statusCode));
                        try {
                            JSONObject datas = new JSONObject(new String(responseBody));
                            Log.e("loginUSer", datas.toString());
                            if (datas.getInt("response") == 1 && statusCode == 200){
//                                Toast.makeText(LoginActivity.this, "test", Toast.LENGTH_LONG).show();
                                JSONArray profileUser = datas.getJSONArray("data");
                                for (int i = 0; i < profileUser.length(); i++) {
                                    JSONObject x = profileUser.getJSONObject(i);
                                    idProfile = x.getString("id_profile_user");
                                    idAkun = x.getString("id_akun");
                                    nama = x.getString("nama");
                                    jk = x.getString("jk");
                                    noHp = x.getString("no_hp");
                                    email = x.getString("email");
                                    foto = x.getString("foto");
                                    alamat = x.getString("alamat");
                                    username = x.getString("username");
                                    pass = x.getString("password");
                                    level = x.getString("level");
                                    status = x.getString("status");
                                }
                                UserSession x = new UserSession(LoginActivity.this);
                                x.setIsLogin(true);
                                x.setIdProfileUser(idProfile);
                                x.setIdAkun(idAkun);
                                x.setNama(nama);
                                x.setJk(jk);
                                x.setNoHp(noHp);
                                x.setEmail(email);
                                if (foto.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                                    x.setFoto(null);
                                } else {
                                    x.setFoto(foto);
                                }
                                Log.e("alamat", String.valueOf(alamat.length()));
                                if (alamat.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                                    x.setAlamat(null);
                                } else {
                                    x.setAlamat(alamat);
                                }
                                x.setUsername(username);
                                x.setPassword(pass);
                                x.setLevel(level);
                                x.setStatus(status);

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                Toast.makeText(getApplicationContext(), "Hai "+new UserSession(getApplicationContext()).getNama()+", laporkan jika terjadi musibah atau bencana yang ada.",
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        etUsername.setText(null);
                        etPassword.setText(null);
                        pDialogLogin.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("status error", String.valueOf(statusCode));
                        Log.e("error", error.toString());
                        if (statusCode == 400) {
                            try {
                                JSONObject datas = new JSONObject(new String(responseBody));
                                Log.e("loginUSer", datas.toString());
                                if (datas.getInt("response") == 0){
                                    Log.e("status", String.valueOf(statusCode));
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage(datas.getString("message"));
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_alert);
                            dialog.setTitle("Kesalahan");
                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            bsdConn.show();
                        }
                        etUsername.setText(null);
                        etPassword.setText(null);
                        pDialogLogin.dismiss();
                    }

                });
            } else {
                bsdConn.show();
                pDialogLogin.dismiss();
            }
        }

//        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//        startActivity(intent);
//        finish();
    }

    @OnClick(R.id.ll_login_akun)
    public void pindahFiturDaftar(){
        Intent intent = new Intent(LoginActivity.this, DaftraAkunActivity.class);
        startActivity(intent);
        finish();
    }

    private void init_modal_bd_dialog(){
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }
}
