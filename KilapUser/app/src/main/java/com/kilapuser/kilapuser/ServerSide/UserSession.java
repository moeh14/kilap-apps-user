package com.kilapuser.kilapuser.ServerSide;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Created by user on 18-Aug-17.
 */

public class UserSession {

//    private final static UserSession instance;

    static {
//        instance = new UserSession();
    }

//    public static UserSession getInstance() {
//        return instance;
//    }

    private final SharedPreferences appPreference;

    public UserSession(Context context) {
        appPreference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setSession(String key, String value) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void clearSession() {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.clear();
        editor.apply();
        editor.commit();
    }

    public String getSession(String key) {
        String value = appPreference.getString(key, "");
        if (TextUtils.isEmpty(value)) {
            return "";
        }
        return value;
    }

    public void setIsLogin(boolean stat) {
        SharedPreferences.Editor editor = appPreference.edit();
        editor.putBoolean("IS_LOGIN", stat);
        editor.apply();
    }

    public boolean getIsLogin() {
        boolean isLogin = appPreference.getBoolean("IS_LOGIN", false);
        return isLogin;
    }

//    public void setIdProfileUser(String idProfileUser) {
//        setSession("id_profile_user", idProfileUser);
//    }
//
//    public String getIdProfileUser(){
//        return getSession("id_profile_user");
//    }
//
//    public void setIdAkun(String idAkun) {
//        setSession("id_akun", idAkun);
//    }
//
//    public String getIdAkun(){
//        return getSession("id_akun");
//    }
//
//    public void setNama(String nama) {
//        setSession("nama", nama);
//    }
//
//    public String getNama(){
//        return getSession("nama");
//    }
//
//    public void setJnsKelamin(String jk) {
//        setSession("jk", jk);
//    }
//
//    public String getJnsKelamin(){
//        return getSession("jk");
//    }
//
//    public void setNoHp(String noHp) {
//        setSession("no_hp", noHp);
//    }
//
//    public String getKdKaryawan(){
//        return getSession("kdKaryawan");
//    }
//
//    public void setJabatan(String jabatan) {
//        setSession("jabatan", jabatan);
//    }
//
//    public String getJabatan() {
//        return getSession("jabatan");
//    }


    public String getIdProfileUser() {
        return getSession("id_profile_user");
    }

    public void setIdProfileUser(String idProfileUser) {
        setSession("id_profile_user", idProfileUser);
    }

    public String getIdAkun() {
        return getSession("id_akun");
    }

    public void setIdAkun(String idAkun) {
        setSession("id_akun", idAkun);
    }

    public String getNama() {
        return getSession("nama");
    }

    public void setNama(String nama) {
        setSession("nama", nama);;
    }

    public String getJk() {
        return getSession("jk");
    }

    public void setJk(String jk) {
        setSession("jk", jk);
    }

    public String getNoHp() {
        return getSession("no_hp");
    }

    public void setNoHp(String noHp) {
        setSession("no_hp", noHp);
    }

    public String getEmail() {
        return getSession("email");
    }

    public void setEmail(String email) {
        setSession("email", email);
    }

    public String getFoto() {
        return getSession("foto");
    }

    public void setFoto(String foto) {
        setSession("foto", foto);
    }

    public String getAlamat() {
        return getSession("alamat");
    }

    public void setAlamat(String alamat) {
        setSession("alamat", alamat);
    }

    public String getUsername() {
        return getSession("username");
    }

    public void setUsername(String username) {
        setSession("username", username);
    }

    public String getPassword() {
        return getSession("password");
    }

    public void setPassword(String password) {
        setSession("password", password);
    }

    public String getLevel() {
        return getSession("level");
    }

    public void setLevel(String level) {
        setSession("level", level);
    }

    public String getStatus() {
        return getSession("status");
    }

    public void setStatus(String status) {
        setSession("status", status);
    }
}


