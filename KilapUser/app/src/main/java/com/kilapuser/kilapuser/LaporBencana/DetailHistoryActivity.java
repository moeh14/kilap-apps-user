package com.kilapuser.kilapuser.LaporBencana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kilapuser.kilapuser.R;
import com.kilapuser.kilapuser.ServerSide.URL;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailHistoryActivity extends AppCompatActivity {
    //data lapor
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_kat)
    TextView tvKat;
    @BindView(R.id.tv_jns)
    TextView tvJns;
    @BindView(R.id.tv_alamat)
    TextView tvAlamat;
    @BindView(R.id.tv_ket)
    TextView tvKet;
    @BindView(R.id.iv_bencana)
    ImageView ivBencana;
    @BindView(R.id.pb_load_image)
    ProgressBar pbImage;
    @BindView(R.id.tv_tgl_bencana)
    TextView tvTglBencana;
    @BindView(R.id.tv_tgl_lapor)
    TextView tvTglLapor;
    //hasil
    @BindView(R.id.tv_atas_nama)
    TextView tvAtasNama;
    @BindView(R.id.ll_atas_nama)
    LinearLayout llAtasNama;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.ll_status)
    LinearLayout llStatus;
    @BindView(R.id.tv_tgl_hasil)
    TextView tvTglHasil;
    @BindView(R.id.ll_tgl_hasil)
    LinearLayout llTglHasil;
    @BindView(R.id.tv_ket_hasil)
    TextView tvKetHasil;
    @BindView(R.id.ll_ket_hasil)
    LinearLayout llKetHasil;
    @BindView(R.id.iv_hasil)
    ImageView ivHasil;
    @BindView(R.id.pb_load_hasil)
    ProgressBar pbHasil;
    @BindView(R.id.rl_hasil)
    RelativeLayout rlHasil;
    @BindView(R.id.ll_image_hasil)
    LinearLayout llImageHasil;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    private String id, kat, jns, alamat, ket, image, tglBencana, tglLapor, nama, tglHasil, ketHasil, imageHasil;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history);
        ButterKnife.bind(this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("DETAIL HISTORY");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        id = intent.getExtras().getString("id");
        kat = intent.getExtras().getString("kat");
        jns = intent.getExtras().getString("jns");
        alamat = intent.getExtras().getString("alamat");
        ket = intent.getExtras().getString("ket");
        image = intent.getExtras().getString("image");
        tglBencana = intent.getExtras().getString("tgl_bencana");
        tglLapor = intent.getExtras().getString("tgl_lapor");
        nama = intent.getExtras().getString("nama");
        status =  intent.getExtras().getInt("status");
        tglHasil = intent.getExtras().getString("tgl_hasil");
        ketHasil = intent.getExtras().getString("ket_hasil");
        imageHasil = intent.getExtras().getString("image_hasil");

        Log.e("nama", ket);
        tvId.setText(id);
        tvKat.setText(kat);
        tvJns.setText(jns);
        tvAlamat.setText(alamat);
        tvKet.setText(ket);

        Picasso.with(getApplicationContext()).load(URL.rootImage + image)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(ivBencana, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
                        pbImage.setVisibility(View.GONE);
                        ivBencana.setImageResource(R.drawable.ic_no_image);
                    }
                });
        tvTglBencana.setText(tglBencana);
        tvTglLapor.setText(tglLapor);
        if (status == 0) {
            llAtasNama.setVisibility(View.GONE);
            tvAtasNama.setText("-");
            llStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Lapron belum diterima");
            llTglHasil.setVisibility(View.GONE);
            tvTglHasil.setText("-");
            llKetHasil.setVisibility(View.GONE);
            tvKetHasil.setText("-");
            llImageHasil.setVisibility(View.GONE);
            ivHasil.setImageResource(R.drawable.ic_no_image);
        } else if (status == 1){
            llAtasNama.setVisibility(View.VISIBLE);
            tvAtasNama.setText(nama);
            llStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Laporan sudah diterima");
            llTglHasil.setVisibility(View.VISIBLE);
            tvTglHasil.setText(tglHasil);
            llKetHasil.setVisibility(View.GONE);
            tvKetHasil.setText("-");
            llImageHasil.setVisibility(View.GONE);
            ivHasil.setImageResource(R.drawable.ic_no_image);
        } else if (status == 2) {
            llAtasNama.setVisibility(View.VISIBLE);
            tvAtasNama.setText(nama);
            llStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Laporan sedang proses ditindaklanjuti");
            llTglHasil.setVisibility(View.VISIBLE);
            tvTglHasil.setText(tglHasil);
            llKetHasil.setVisibility(View.VISIBLE);
            tvKetHasil.setText(ketHasil);
            llImageHasil.setVisibility(View.GONE);
            ivHasil.setImageResource(R.drawable.ic_no_image);
        } else if (status == 3) {
            llAtasNama.setVisibility(View.VISIBLE);
            tvAtasNama.setText(nama);
            llStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Laporan telah selesai ditindaklanjuti");
            llTglHasil.setVisibility(View.VISIBLE);
            tvTglHasil.setText(tglHasil);
            llKetHasil.setVisibility(View.VISIBLE);
            tvKetHasil.setText(ketHasil);
            llImageHasil.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(URL.rootImage + imageHasil)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(ivHasil, new Callback() {
                        @Override
                        public void onSuccess() {
                            pbHasil.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
                            pbHasil.setVisibility(View.GONE);
                            ivHasil.setImageResource(R.drawable.ic_no_image);
                        }
                    });
        } else {
            llAtasNama.setVisibility(View.VISIBLE);
            tvAtasNama.setText(nama);
            llStatus.setVisibility(View.VISIBLE);
            tvStatus.setText("Laporan tidak diterima");
            llTglHasil.setVisibility(View.VISIBLE);
            tvTglHasil.setText(tglHasil);
            llKetHasil.setVisibility(View.VISIBLE);
            tvKetHasil.setText(ketHasil);
            llImageHasil.setVisibility(View.GONE);
            ivHasil.setImageResource(R.drawable.ic_no_image);
        }
    }
}
