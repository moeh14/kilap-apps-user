package com.kilapuser.kilapuser.ServerSide;

public class URL {
    public static final String root = "http://kilapapi.bdigy.com/";
//    public static final String root = "http://kilap.planetinformatika.com/";
    public static final String rootImage = "http://kilapsite.bdigy.com/assets/upload/image/";
    public static final String loginUser = root+"loginUser";
    public static final String getDataUser = root+"dataUser/";
    public static final String daftarUser = root+"daftarAkunUser";
    public static final String getMasterBencana = root+"getMasterBencana";
    public static final String laporBencana = root+"laporUser";
    public static final String historyBencana = root+"historyBencanaUser/";
    public static final String beritaBencana = root+"beritaBencana";
    public static final String updateProfileUser = root+"updateProfileUser";
    public static final String ubahPassword = root+"ubahPassword";
    public static final String updateImgUser = root+"updateImgUser";
    public static final String ambilOrderan = root+"ambilOrderan/";
    public static final String mulaiPekerjaan = root+"mulaiPekerjaan/";
    public static final String orderanSelesai = root+"orderanSelesai/";
    public static final String dataPesanan = root+"dataPesanan/";
    //public static final String mejaPesan = root+"mejaPesan.php";
//    public static final String root_image_makan = rootImage+"dist/img/";
    public static final int TIMEOUT_ACCESS = 10000;
}